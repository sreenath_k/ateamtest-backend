<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use File;
use DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function guard()
    {
        return Auth::guard('api');
    }

    public function getUsers(Request $request) {
        if(!$user = $this->checkToken($request)){
            return response()->json([
                'status' => false,
                'data' => null,
                'message' => 'Authorization required'
            ]);
        }
        else{
            $users = User::where('id', '!=', $user->id)->get();
            return response()->json([
                'status' => true,
                'data' => $users,
                'message' => 'Users fetched successfully'
            ]);
        }
    }
    public function getFriends(Request $request) {
        if(!$user = $this->checkToken($request)){
            return response()->json([
                'status' => false,
                'data' => null,
                'message' => 'Authorization required'
            ]);
        }
        else{
            $friends = DB::table('user_friend_tbl')
                ->join('users', 'users.id', '=', 'user_friend_tbl.friendId')
                ->where('user_friend_tbl.userId', $user->id)
                ->select('users.*')
                ->get();
            return response()->json([
                'status' => true,
                'data' => $friends,
                'message' => 'Friends fetched successfully'
            ]);
        }
    }
    public function userLogin(Request $request) {

        $email = $request->input('email');
        $password = $request->input('password');

        if($email != '' AND $password != ''){

            $user = User::where('email', $email)->get()->first();
            if($user){
                if(Hash::check($password, $user->password)) {
                    $user = $this->updateApiToken($email);
                    return response()->json([
                        'status' => true,
                        'data' => $user,
                        'message' => 'Logged in successfully'
                    ]);
                }
                else{
                    return response()->json([
                        'status' => false,
                        'data' => null,
                        'message' => 'Password is wrong'
                    ]);
                }
            }
            else{
                return response()->json([
                    'status' => false,
                    'data' => null,
                    'message' => 'Email or password is wrong'
                ]);
            }
        }
        else{
            return response()->json([
                'status' => false,
                'data' => null,
                'message' => 'Please pass all fields'
            ]);
        }
    }
    public function userRegister(Request $request) {

        $name = $request->input('name');
        $email = $request->input('email');
        $password = $request->input('password');
        $file = $request->file('dp');

        if($name != '' AND $email != '' AND $password != '' AND $file != null){

            if($count = $this->isEmailExists($email)){
                return response()->json([
                    'status' => false,
                    'data' => null,
                    'message' => 'Email exists'
                ]);
            }
            if($request->hasFile('dp')){
                $filenameWithExt = $file->getClientOriginalName();
                $fileName = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                $extension = $file->getClientOriginalExtension();
                $fileNameToStore = $fileName.'_'.time().'.'.$extension;
                $file->move(public_path() . '/dp', $fileNameToStore);
                $fileNameDb = '/dp/'.$fileNameToStore;
            }

            $user = new User;
            $user->name = $name;
            $user->email = $email;
            $user->password = Hash::make($password);
            $user->api_token = Str::random(60);
            $user->dp = $fileNameDb;
            $user->save();

            return response()->json([
                'status' => true,
                'data' => $user,
                'message' => 'Created successfully'
            ]);
        }
        else{
            return response()->json([
                'status' => false,
                'data' => null,
                'message' => 'Please pass all fields'
            ]);
        }
    }
    public function userLogout(Request $request){
        $token = $request->input('token');
        if($token != ''){
            $user = User::where('api_token', $token)->get()->first();
            if($user){
                $user->api_token = Str::random(60);
                $user->save();
                return response()->json([
                    'status' => true,
                    'data' => null,
                    'message' => 'Logged out'
                ]);
            }
            else{
                return response()->json([
                    'status' => false,
                    'data' => null,
                    'message' => 'No user found'
                ]);
            }
        }
        else{
            return response()->json([
                'status' => false,
                'data' => null,
                'message' => 'Please pass token'
            ]);
        }
    }
    public function sendRequest(Request $request) {
        if(!$user = $this->checkToken($request)){
            return response()->json([
                'status' => false,
                'data' => null,
                'message' => 'Authorization required'
            ]);
        }

        $friendId = $request->input('friendId');
        $userId = $user->id;

        if($friendId != ''){
            $res = DB::table('user_friend_tbl')->
                where('userId', $userId)->
                where('friendId', $friendId)->
                count();

            if($res<=0){
                $res = DB::table('user_friend_tbl')->insert(
                    ['userId' => $userId, 
                    'friendId' => $friendId, 
                    'isAccepted' => 0,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    ]
                );
                return response()->json([
                    'status' => true,
                    'data' => $res,
                    'message' => 'Requested successfully'
                ]);
            }
            else{
                return response()->json([
                    'status' => false,
                    'data' => null,
                    'message' => 'Already requested'
                ]);
            }
        }
        else{
            return response()->json([
                'status' => false,
                'data' => null,
                'message' => 'Please pass all fields'
            ]);
        }
    }
    public function authenticateUser(Request $request){
        if($user = $this->checkToken($request)){
            return response()->json([
                'status' => true,
                'data' => $user,
                'message' => 'Valid user'
            ]);
        }
        else{
            return response()->json([
                'status' => false,
                'data' => null,
                'message' => 'Logged out'
            ]);
        }
    }
    public function editUser(Request $request) {
        $user = User::where('userId', $request->input('userId'))->first();
        $user->subUserId=$request->input('subUserId');
        $user->userName=$request->input('userName');
        $user->save();

        return response()->json([
            'data' => $user
        ]);
    }
    public function deleteUser(Request $request) {
        $user = User::where('userId', $request->input('userId'))->delete();

        return response()->json([
            'data' => $user
        ]);
    }
    public function isEmailExists($email){
        $count = User::where('email', $email)->count();
        if($count >0){
            return true;
        }
        else{
            return false;
        }
    }
    public function updateApiToken($email){
        $user = User::where('email', $email)->first();
        $user->api_token = Str::random(60);
        $user->save();
        return $user;
    }
    public function checkToken(Request $request){
        $token = $request->header('Authorization');
        $user = User::where('api_token', $token)->first();
        if($user){
            return $user;
        }
        else{
            return false;
        }
    }
}

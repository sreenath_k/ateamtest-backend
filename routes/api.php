<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group([

    'middleware' => 'api',

], function ($router) {
	header('Access-Control-Allow-Origin: *');
	header("Access-Control-Allow-Headers: Authorization");

    //Users
    Route::get('getUsers', 'AuthController@getUsers');
    Route::post('userRegister', 'AuthController@userRegister');
    Route::post('userLogin', 'AuthController@userLogin');
    Route::post('checkToken', 'AuthController@checkToken');
    Route::post('authenticateUser', 'AuthController@authenticateUser');
    Route::post('userLogout', 'AuthController@userLogout');
    Route::post('editUser', 'AuthController@editUser');
    Route::post('deleteUser', 'AuthController@deleteUser');

    Route::get('getFriends', 'AuthController@getFriends');
    Route::post('sendRequest', 'AuthController@sendRequest');

});